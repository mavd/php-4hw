<?php
  $user = "Клим";
  $age = "25";
  $location = "Сочи";
  $email = "i@klim.msk.ru";
  $aboutme_short = "Сноубордист, осваиваю PHP и JS"; // для тега Title
  $aboutme = "Осваиваю PHP и JS. Люблю жену. Сноубордист. Учусь делать правильный дизайн мобильных приложений. <br/>Изучаю JS с целью погружения в MobileDev через ReactNative.js, а PHP для быстрого бэкенда. Вдохновлён Дуровым и Сноуденом."
?>

<html>
<head>
  <title> <?= $user.' - '.$aboutme_short?> </title>
  <style media="screen">
  /*оформление вывода */
    *
    {
      font-family: TrebuchetMS;
      font-size: 1em;
    }

    h1 {
      font-size: 2em;
    }
    dl {
      margin-bottom:50px;
    }
    .desc {
      height: 2em; /* для рубрики Описания */
    }
    dl dt {
      background:gray;
      color:white;
      float:left;
      font-weight:100;
      margin-right:10px;
      padding:5px;
      width:10em;
    }

    dl dd {
      margin:2px 0;
      padding:5px 0;
    }
  </style>
</head>
<article>
    <h1> Страница пользователя <?= $user ?> </h1>
    <dl>
    <dt>Имя: </dt>
    <dd><?= $user ?></dd>

    <dt>Возраст: </dt>
    <dd><?= $age ?></dd>

    <dt>Электропочта:</dt>
    <dd><?= $email ?></dd>

    <dt>Город: </dt>
    <dd><?= $location ?></dd>

    <dt class="desc">Кратко о себе:</dt>
    <dd><?= $aboutme ?></dd>
</dl>

</article>

</html>
